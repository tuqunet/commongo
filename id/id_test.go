package id

import (
	"fmt"
	"testing"
	"time"
)

func TestNextId(t *testing.T) {
	startTime := time.Now() // 记录开始时间

	generator := Generator("test")

	// 这里是你要计时的代码块
	for i := 0; i < 10000; i++ {
		// 模拟耗时操作
		id := generator.NextId()

		if id == 0 {
			t.Error("id is 0")
		}
	}

	endTime := time.Now()             // 记录结束时间
	elapsed := endTime.Sub(startTime) // 计算时间差
	fmt.Printf("耗时: %v\n", elapsed)

	id := generator.NextId()
	fmt.Println("id", id)
}

func TestNextIdInner(t *testing.T) {
	startTime := time.Now() // 记录开始时间

	// 这里是你要计时的代码块
	for i := 0; i < 10000; i++ {
		generator := Generator("test")
		// 模拟耗时操作
		id := generator.NextId()

		if id == 0 {
			t.Error("id is 0")
		}
	}

	endTime := time.Now()             // 记录结束时间
	elapsed := endTime.Sub(startTime) // 计算时间差
	fmt.Printf("耗时: %v\n", elapsed)
}
