package id

import (
	"context"
	"gitee.com/tuqunet/commongo/extvar"
	"github.com/gogf/gf/v2/os/gcache"
	"github.com/sony/sonyflake"
	"strconv"
	"time"
)

var (
	idCache   = gcache.New(10000)
	cxt       = context.Background()
	startTime = time.Date(2024, 1, 1, 0, 0, 0, 0, time.UTC)
)

type Gen struct {
	snowflake *sonyflake.Sonyflake
}

// Generator 获取下一个id值
func Generator(bizCode extvar.BizCode) *Gen {
	cacheVar := idCache.MustGetOrSetFuncLock(cxt, bizCode, func(ctx context.Context) (interface{}, error) {
		snowflake := sonyflake.NewSonyflake(sonyflake.Settings{
			StartTime: startTime,
		})
		return &Gen{snowflake: snowflake}, nil
	}, 0)

	return cacheVar.Val().(*Gen)
}

// NextId 获取下一个id值
func (idMaker *Gen) NextId() uint64 {
	id, err := idMaker.snowflake.NextID()
	if err != nil {
		panic(err)
	}
	return id
}

// NextIdStr 获取下一个id值
func (idMaker *Gen) NextIdStr() string {
	id, err := idMaker.snowflake.NextID()
	if err != nil {
		panic(err)
	}
	return strconv.FormatUint(id, 10)
}
