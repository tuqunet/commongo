package mjwt

import (
	"context"
	"crypto/rsa"
	"github.com/gogf/gf/v2/os/gcache"
	"github.com/golang-jwt/jwt/v5"
	"time"
)

// ManageJwt provides a Json-Web-Token authentication implementation. On failure, a 401 HTTP response
// is returned. On success, the wrapped middleware is called, and the userID is made available as
// c.Get("userID").(string).
// Users can get a token by posting a json request to LoginHandler. The token then needs to be passed in
// the Authentication header. Example: Authorization:Bearer XXX_TOKEN_XXX
// 管理台Jwt配置对象
type ManageJwt struct {
	//登录路径
	LoginPaths []string
	//退出路径
	LogoutPaths []string

	// Realm name to display to the user. Required.
	Realm string

	// signing algorithm - possible values are HS256, HS384, HS512, RS256, RS384 or RS512
	// Optional, default is HS256.
	SigningAlgorithm string

	// Secret key used for signing. Required.
	Key []byte

	// Callback to retrieve key used for signing. Setting KeyFunc will bypass
	// all other key settings
	KeyFunc func(token *jwt.Token) (interface{}, error)

	// Duration that a jwt token is valid. Optional, defaults to one hour.
	Timeout time.Duration

	// This field allows clients to refresh their token until MaxRefresh has passed.
	// Note that clients can refresh their token in the last moment of MaxRefresh.
	// This means that the maximum validity timespan for a token is TokenTime + MaxRefresh.
	// Optional, defaults to 0 meaning not refreshable.
	MaxRefresh time.Duration

	// 认证操作
	// Callback function that should perform the authentication of the user based on login info.
	// Must return user data as user identifier, it will be stored in Claim Array. Required.
	// Check error (e) to determine the appropriate error message.
	Authenticator func(ctx context.Context) (interface{}, error)

	// 是否授权通过，认证成功后判断是否还需要权限判断
	// Callback function that should perform the authorization of the authenticated user. Called
	// only after an authentication success. Must return true on success, false on failure.
	// Optional, default to success.
	Authorizer func(data interface{}, ctx context.Context) bool

	// Callback function that will be called during login.
	// Using this function it is possible to add additional payload data to the web token.
	// The data is then made available during requests via c.Get(jwt.PayloadKey).
	// Note that the payload is not encrypted.
	// The attributes mentioned on jwt.io can't be used as keys for the map.
	// Optional, by default no additional data will be set.
	PayloadFunc func(data interface{}) MapClaims

	// User can define own Unauthorized func.
	// 没有通过授权时的处理方法
	Unauthorized func(ctx context.Context, code int, message string)

	// Set the identity handler function
	// 身份标识提取函数
	IdentityHandler func(ctx context.Context) interface{}

	// Set the identity key
	// 身份标识键,用于提取身份标识
	IdentityKey string

	// TokenLookup is a string in the form of "<source>:<name>" that is used
	// to extract token from the request.
	// Optional. Default value "header:Authorization".
	// Possible values:
	// - "header:<name>"
	// - "query:<name>"
	// - "cookie:<name>"
	TokenLookups []string

	// TokenHeadName is a string in the header. Default value is "Bearer"
	TokenHeadName string

	// TimeFunc provides the current time. You can override it to use another time value. This is useful for testing or if your server uses a different time zone than your tokens.
	TimeFunc func() time.Time

	// HTTP Status messages for when something in the JWT middleware fails.
	// Check error (e) to determine the appropriate error message.
	HTTPStatusMessageFunc func(e error, ctx context.Context) string

	// Private key file for asymmetric algorithms
	PrivateKeyFile string

	// Private Key bytes for asymmetric algorithms
	//
	// Note: PrivateKeyFile takes precedence over PrivateKeyBytes if both are set
	PrivateKeyBytes []byte

	// Public key file for asymmetric algorithms
	PubKeyFile string

	// Private key passphrase
	PrivateKeyPassphrase string

	// Public key bytes for asymmetric algorithms.
	//
	// Note: PubKeyFile takes precedence over PubKeyBytes if both are set
	PubKeyBytes []byte

	// Private key
	privateKey *rsa.PrivateKey

	// Public key
	publicKey *rsa.PublicKey

	// Optionally return the token as a cookie
	SendCookie bool

	// Duration that a cookie is valid. Optional, by default equals to Timeout value.
	CookieMaxAge time.Duration

	// Allow insecure cookies for development over http
	SecureCookie bool

	// Allow cookies to be accessed client side for development
	CookieHTTPOnly bool

	// Allow cookie domain change for development
	CookieDomain string

	// SendAuthorization allow return authorization header for every request
	SendAuthorization bool

	// Disable abort() of context.
	DisabledAbort bool

	// CookieName allow cookie name change for development
	CookieName string

	// CacheAdapter
	CacheAdapter gcache.Adapter

	// BlacklistPrefix
	BlacklistPrefix string

	//排除的路径
	ExcludePaths []string
}

// Option 定义函数选项类型
type Option func(cfg *ManageJwt)

// WithLoginPaths 设置登录路径
func WithLoginPaths(loginPaths ...string) Option {
	return func(cfg *ManageJwt) {
		cfg.LoginPaths = loginPaths
	}
}

// WithLogoutPaths 设置退出路径
func WithLogoutPaths(logoutPaths ...string) Option {
	return func(cfg *ManageJwt) {
		cfg.LogoutPaths = logoutPaths
	}
}

// WithRealm 设置Realm
func WithRealm(realm string) Option {
	return func(cfg *ManageJwt) {
		cfg.Realm = realm
	}
}

// WithSigningAlgorithm 设置签名算法
func WithSigningAlgorithm(signingAlgorithm string) Option {
	return func(cfg *ManageJwt) {
		cfg.SigningAlgorithm = signingAlgorithm
	}
}

// WithKey 设置密钥
func WithKey(key []byte) Option {
	return func(cfg *ManageJwt) {
		cfg.Key = key
	}
}

// WithKeyFunc 设置解析token处理方法
func WithKeyFunc(keyFunc func(token *jwt.Token) (interface{}, error)) Option {
	return func(cfg *ManageJwt) {
		cfg.KeyFunc = keyFunc
	}
}

// WithTimeout 设置token超时时间
func WithTimeout(timeout time.Duration) Option {
	return func(cfg *ManageJwt) {
		cfg.Timeout = timeout
	}
}

// WithMaxRefresh 设置token可刷新时间
func WithMaxRefresh(maxRefresh time.Duration) Option {
	return func(cfg *ManageJwt) {
		cfg.MaxRefresh = maxRefresh
	}
}

// WithAuthenticator 设置认证操作
func WithAuthenticator(authenticator func(ctx context.Context) (interface{}, error)) Option {
	return func(cfg *ManageJwt) {
		cfg.Authenticator = authenticator
	}
}

// WithAuthorizer 设置授权操作
func WithAuthorizer(authorizer func(data interface{}, ctx context.Context) bool) Option {
	return func(cfg *ManageJwt) {
		cfg.Authorizer = authorizer
	}
}

// WithPayloadFunc payload处理方法
func WithPayloadFunc(payloadFunc func(data interface{}) MapClaims) Option {
	return func(cfg *ManageJwt) {
		cfg.PayloadFunc = payloadFunc
	}
}
