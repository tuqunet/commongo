package mjwt

import (
	"context"
	"errors"
	"github.com/gogf/gf/v2/os/glog"
	"github.com/gogf/gf/v2/text/gstr"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gogf/gf/v2/crypto/gmd5"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/net/ghttp"
	"github.com/gogf/gf/v2/os/gcache"
	"github.com/golang-jwt/jwt/v5"
)

// MapClaims type that uses the map[string]interface{} for JSON decoding
// This is the default claims type if you don't supply one
// jwt token内容
type MapClaims map[string]interface{}

var (
	// TokenKey default jwt token key in params
	TokenKey = "JWT_TOKEN"
	// PayloadKey default jwt payload key in params
	PayloadKey = "JWT_PAYLOAD"
	// IdentityKey default identity key
	IdentityKey = "identity"
	// The blacklist stores tokens that have not expired but have been deactivated.
	blacklist = gcache.New()
)

// New for check error with ManageJwt
func New(options ...Option) *ManageJwt {
	mw := new(ManageJwt)

	// 设置配置值
	for _, option := range options {
		option(mw)
	}

	if len(mw.TokenLookups) == 0 {
		mw.TokenLookups = []string{"header:Authorization"}
	}

	if mw.SigningAlgorithm == "" {
		mw.SigningAlgorithm = "HS256"
	}

	if mw.Timeout == 0 {
		mw.Timeout = time.Hour
	}

	if mw.TimeFunc == nil {
		mw.TimeFunc = time.Now
	}

	mw.TokenHeadName = strings.TrimSpace(mw.TokenHeadName)
	if len(mw.TokenHeadName) == 0 {
		mw.TokenHeadName = "Bearer"
	}

	if mw.Authorizer == nil {
		mw.Authorizer = func(data interface{}, ctx context.Context) bool {
			return true
		}
	}

	if mw.Unauthorized == nil {
		mw.Unauthorized = func(ctx context.Context, code int, message string) {
			r := g.RequestFromCtx(ctx)
			r.Response.WriteJson(g.Map{
				"code":    code,
				"success": code == 0,
				"message": message,
			})
		}
	}

	if mw.IdentityKey == "" {
		mw.IdentityKey = IdentityKey
	}

	if mw.IdentityHandler == nil {
		mw.IdentityHandler = func(ctx context.Context) interface{} {
			claims := ExtractClaims(ctx)
			return claims[mw.IdentityKey]
		}
	}

	if mw.HTTPStatusMessageFunc == nil {
		mw.HTTPStatusMessageFunc = func(e error, ctx context.Context) string {
			return e.Error()
		}
	}

	if mw.Realm == "" {
		mw.Realm = "tuqu jwt"
	}

	if mw.CookieMaxAge == 0 {
		mw.CookieMaxAge = mw.Timeout
	}

	if mw.CookieName == "" {
		mw.CookieName = "jwt"
	}

	// bypass other key settings if KeyFunc is set
	if mw.KeyFunc != nil {
		return nil
	}

	if mw.usingPublicKeyAlgo() {
		if err := mw.readKeys(); err != nil {
			panic(err)
		}
	}

	if mw.Key == nil {
		glog.Error(context.Background(), "JWT加密密钥缺失")
		panic(ErrMissingSecretKey)
	}

	if mw.CacheAdapter != nil {
		blacklist.SetAdapter(mw.CacheAdapter)
	}

	if mw.BlacklistPrefix == "" {
		mw.BlacklistPrefix = "JWT:BLACKLIST:"
	}

	return mw
}

// MiddlewareFunc makes ManageJwt implement the Middleware interface.
func (mw *ManageJwt) MiddlewareFunc() ghttp.HandlerFunc {
	return func(r *ghttp.Request) {
		needAuth := mw.AuthPath(r.GetCtx(), r.URL.Path)
		if needAuth {
			mw.middlewareImpl(r.GetCtx())
		}
		r.Middleware.Next()
	}
}

// AuthPath 判断路径是否需要进行认证拦截
// return true 需要认证
func (mw *ManageJwt) AuthPath(ctx context.Context, urlPath string) bool {
	// 去除后斜杠
	if strings.HasSuffix(urlPath, "/") {
		urlPath = gstr.SubStr(urlPath, 0, len(urlPath)-1)
	}
	// 分组拦截，登录接口不拦截
	if mw.LoginPaths != nil {
		for _, loginPath := range mw.LoginPaths {
			if gstr.HasSuffix(urlPath, loginPath) {
				return false
			}
		}
	}

	// 排除路径处理，到这里nextFlag为true
	for _, excludePath := range mw.ExcludePaths {
		tmpPath := excludePath
		// 前缀匹配
		if strings.HasSuffix(tmpPath, "/*") {
			tmpPath = gstr.SubStr(tmpPath, 0, len(tmpPath)-2)
			if gstr.HasPrefix(urlPath, tmpPath) {
				// 前缀匹配不拦截
				return false
			}
		} else {
			// 全路径匹配
			if strings.HasSuffix(tmpPath, "/") {
				tmpPath = gstr.SubStr(tmpPath, 0, len(tmpPath)-1)
			}
			if urlPath == tmpPath {
				// 全路径匹配不拦截
				return false
			}
		}
	}

	return true
}

// GetClaimsFromJWT get claims from JWT token
func (mw *ManageJwt) GetClaimsFromJWT(ctx context.Context) (MapClaims, string, error) {
	r := g.RequestFromCtx(ctx)

	token, err := mw.parseToken(r)
	if err != nil {
		return nil, "", err
	}

	if mw.SendAuthorization {
		token := r.Get(TokenKey).String()
		if len(token) > 0 {
			r.Header.Set("Authorization", mw.TokenHeadName+" "+token)
		}
	}

	claims := MapClaims{}
	for key, value := range token.Claims.(jwt.MapClaims) {
		claims[key] = value
	}

	return claims, token.Raw, nil
}

// LoginHandler can be used by clients to get a jwt token.
// Payload needs to be json in the form of {"username": "USERNAME", "password": "PASSWORD"}.
// Reply will be of the form {"token": "TOKEN"}.
func (mw *ManageJwt) LoginHandler(ctx context.Context) (tokenString string, expire time.Time) {
	if mw.Authenticator == nil {
		mw.unauthorized(ctx, http.StatusInternalServerError, mw.HTTPStatusMessageFunc(ErrMissingAuthenticatorFunc, ctx))
		return
	}

	data, err := mw.Authenticator(ctx)
	if err != nil {
		mw.unauthorized(ctx, http.StatusUnauthorized, mw.HTTPStatusMessageFunc(err, ctx))
		return
	}

	r := g.RequestFromCtx(ctx)
	// Create the token
	token := jwt.New(jwt.GetSigningMethod(mw.SigningAlgorithm))
	claims := token.Claims.(jwt.MapClaims)

	if mw.PayloadFunc != nil {
		for key, value := range mw.PayloadFunc(data) {
			claims[key] = value
		}
	}

	if _, ok := claims[mw.IdentityKey]; !ok {
		mw.unauthorized(ctx, http.StatusInternalServerError, mw.HTTPStatusMessageFunc(ErrMissingIdentity, ctx))
		return
	}

	expire = mw.TimeFunc().Add(mw.Timeout)
	claims["exp"] = expire.UnixNano() / 1e6
	claims["orig_iat"] = mw.TimeFunc().UnixNano() / 1e6

	tokenString, err = mw.signedString(token)
	if err != nil {
		mw.unauthorized(ctx, http.StatusUnauthorized, mw.HTTPStatusMessageFunc(ErrFailedTokenCreation, ctx))
		return
	}

	// set cookie
	if mw.SendCookie {
		expireCookie := mw.TimeFunc().Add(mw.CookieMaxAge)
		maxAge := (expireCookie.UnixNano() - mw.TimeFunc().UnixNano()) / 1e6
		r.Cookie.SetCookie(mw.CookieName, tokenString, mw.CookieDomain, "/", time.Duration(maxAge)*time.Second)
	}

	return
}

// LogoutHandler can be used by clients to remove the jwt cookie (if set)
func (mw *ManageJwt) LogoutHandler(ctx context.Context) {
	r := g.RequestFromCtx(ctx)

	// delete auth cookie
	if mw.SendCookie {
		r.Cookie.SetCookie(mw.CookieName, "", mw.CookieDomain, "/", -1)
	}

	claims, token, err := mw.CheckIfTokenExpire(ctx)
	if err != nil {
		mw.unauthorized(ctx, http.StatusUnauthorized, mw.HTTPStatusMessageFunc(err, ctx))
		return
	}

	err = mw.setBlacklist(ctx, token, claims)

	if err != nil {
		mw.unauthorized(ctx, http.StatusUnauthorized, mw.HTTPStatusMessageFunc(err, ctx))
		return
	}

	return
}

// RefreshHandler can be used to refresh a token. The token still needs to be valid on refresh.
// Shall be put under an endpoint that is using the ManageJwt.
// Reply will be of the form {"token": "TOKEN"}.
func (mw *ManageJwt) RefreshHandler(ctx context.Context) (tokenString string, expire time.Time) {
	tokenString, expire, err := mw.RefreshToken(ctx)
	if err != nil {
		mw.unauthorized(ctx, http.StatusUnauthorized, mw.HTTPStatusMessageFunc(err, ctx))
		return
	}

	return
}

// RefreshToken refresh token and check if token is expired
func (mw *ManageJwt) RefreshToken(ctx context.Context) (string, time.Time, error) {
	claims, token, err := mw.CheckIfTokenExpire(ctx)
	if err != nil {
		return "", time.Now(), err
	}

	r := g.RequestFromCtx(ctx)
	// Create the token
	newToken := jwt.New(jwt.GetSigningMethod(mw.SigningAlgorithm))
	newClaims := newToken.Claims.(jwt.MapClaims)

	for key := range claims {
		newClaims[key] = claims[key]
	}

	expire := mw.TimeFunc().Add(mw.Timeout)
	newClaims["exp"] = expire.UnixNano() / 1e6
	newClaims["orig_iat"] = mw.TimeFunc().UnixNano() / 1e6
	tokenString, err := mw.signedString(newToken)
	if err != nil {
		return "", time.Now(), err
	}

	// set cookie
	if mw.SendCookie {
		expireCookie := mw.TimeFunc().Add(mw.CookieMaxAge)
		maxAge := (expireCookie.UnixNano() - time.Now().UnixNano()) / 1e6
		r.Cookie.SetCookie(mw.CookieName, tokenString, mw.CookieDomain, "/", time.Duration(maxAge)*time.Second)
	}

	// set old token in blacklist
	err = mw.setBlacklist(ctx, token, claims)
	if err != nil {
		return "", time.Now(), err
	}

	return tokenString, expire, nil
}

// CheckIfTokenExpire check if token expire
func (mw *ManageJwt) CheckIfTokenExpire(ctx context.Context) (jwt.MapClaims, string, error) {
	r := g.RequestFromCtx(ctx)

	token, err := mw.parseToken(r)
	if err != nil {
		// If we receive an error, and the error is anything other than a single
		// ValidationErrorExpired, we want to return the error.
		// If the error is just ValidationErrorExpired, we want to continue, as we can still
		// refresh the token if it's within the MaxRefresh time.
		// (see https://github.com/appleboy/gin-jwt/issues/176)
		if errors.Is(err, jwt.ErrTokenExpired) {
			return nil, "", err
		}
	}

	in, err := mw.inBlacklist(ctx, token.Raw)
	if err != nil {
		return nil, "", err
	}
	if in {
		return nil, "", ErrInvalidToken
	}

	claims := token.Claims.(jwt.MapClaims)

	exp := int64(claims["exp"].(float64))

	if exp < (mw.TimeFunc().Add(-mw.MaxRefresh).UnixNano() / 1e6) {
		return nil, "", ErrExpiredToken
	}

	return claims, token.Raw, nil
}

// TokenGenerator method that clients can use to get a jwt token.
func (mw *ManageJwt) TokenGenerator(data interface{}) (string, time.Time, error) {
	token := jwt.New(jwt.GetSigningMethod(mw.SigningAlgorithm))
	claims := token.Claims.(jwt.MapClaims)

	if mw.PayloadFunc != nil {
		for key, value := range mw.PayloadFunc(data) {
			claims[key] = value
		}
	}

	expire := mw.TimeFunc().UTC().Add(mw.Timeout)
	claims["exp"] = expire.UnixNano() / 1e6
	claims["orig_iat"] = mw.TimeFunc().UnixNano() / 1e6
	tokenString, err := mw.signedString(token)
	if err != nil {
		return "", time.Time{}, err
	}

	return tokenString, expire, nil
}

// GetToken help to get the JWT token string
//func (mw *ManageJwt) GetToken(ctx context.Context) string {
//	r := g.RequestFromCtx(ctx)
//	token := r.Get(TokenKey).String()
//	if len(token) == 0 {
//		return ""
//	}
//	return token
//}

// GetPayload help to get the payload map
//func (mw *ManageJwt) GetPayload(ctx context.Context) string {
//	r := g.RequestFromCtx(ctx)
//	token := r.Get(PayloadKey).String()
//	if len(token) == 0 {
//		return ""
//	}
//	return token
//}

// GetIdentity help to get the identity
//func (mw *ManageJwt) GetIdentity(ctx context.Context) interface{} {
//	r := g.RequestFromCtx(ctx)
//	return r.Get(mw.IdentityKey)
//}

// ExtractClaims help to extract the JWT claims
func ExtractClaims(ctx context.Context) MapClaims {
	r := g.RequestFromCtx(ctx)
	claims := r.GetParam(PayloadKey).Interface()
	return claims.(MapClaims)
}

// ExtractClaimsFromToken help to extract the JWT claims from token
func ExtractClaimsFromToken(token *jwt.Token) MapClaims {
	if token == nil {
		return make(MapClaims)
	}

	claims := MapClaims{}
	for key, value := range token.Claims.(jwt.MapClaims) {
		claims[key] = value
	}

	return claims
}

// ================= private func =================
func (mw *ManageJwt) readKeys() error {
	err := mw.readPrivateKey()
	if err != nil {
		return err
	}
	err = mw.readPublicKey()
	if err != nil {
		return err
	}
	return nil
}

func (mw *ManageJwt) readPrivateKey() error {
	var keyData []byte
	if mw.PrivateKeyFile == "" {
		keyData = mw.PrivateKeyBytes
	} else {
		fileContent, err := os.ReadFile(mw.PrivateKeyFile)
		if err != nil {
			return ErrNoPrivateKeyFile
		}
		keyData = fileContent
	}

	if mw.PrivateKeyPassphrase != "" {
		//nolint:static check
		key, err := jwt.ParseRSAPrivateKeyFromPEMWithPassword(keyData, mw.PrivateKeyPassphrase)
		if err != nil {
			return ErrInvalidPrivateKey
		}
		mw.privateKey = key
		return nil
	}

	key, err := jwt.ParseRSAPrivateKeyFromPEM(keyData)
	if err != nil {
		return ErrInvalidPrivateKey
	}
	mw.privateKey = key
	return nil
}

func (mw *ManageJwt) readPublicKey() error {
	var keyData []byte
	if mw.PubKeyFile == "" {
		keyData = mw.PubKeyBytes
	} else {
		fileContent, err := ioutil.ReadFile(mw.PubKeyFile)
		if err != nil {
			return ErrNoPubKeyFile
		}
		keyData = fileContent
	}

	key, err := jwt.ParseRSAPublicKeyFromPEM(keyData)
	if err != nil {
		return ErrInvalidPubKey
	}
	mw.publicKey = key
	return nil
}

func (mw *ManageJwt) usingPublicKeyAlgo() bool {
	switch mw.SigningAlgorithm {
	case "RS256", "RS512", "RS384":
		return true
	}
	return false
}

func (mw *ManageJwt) signedString(token *jwt.Token) (string, error) {
	var tokenString string
	var err error
	if mw.usingPublicKeyAlgo() {
		tokenString, err = token.SignedString(mw.privateKey)
	} else {
		tokenString, err = token.SignedString(mw.Key)
	}
	return tokenString, err
}

func (mw *ManageJwt) jwtFromHeader(r *ghttp.Request, key string) (string, error) {
	authHeader := r.Header.Get(key)

	if authHeader == "" {
		return "", ErrEmptyAuthHeader
	}

	parts := strings.SplitN(authHeader, " ", 2)
	if !(len(parts) == 2 && parts[0] == mw.TokenHeadName) {
		return "", ErrInvalidAuthHeader
	}

	return parts[1], nil
}

func (mw *ManageJwt) jwtFromQuery(r *ghttp.Request, key string) (string, error) {
	token := r.Get(key).String()

	if token == "" {
		return "", ErrEmptyQueryToken
	}

	return token, nil
}

func (mw *ManageJwt) jwtFromCookie(r *ghttp.Request, key string) (string, error) {
	cookie := r.Cookie.Get(key).String()

	if cookie == "" {
		return "", ErrEmptyCookieToken
	}

	return cookie, nil
}

func (mw *ManageJwt) jwtFromParam(r *ghttp.Request, key string) (string, error) {
	token := r.Get(key).String()

	if token == "" {
		return "", ErrEmptyParamToken
	}

	return token, nil
}

func (mw *ManageJwt) parseToken(r *ghttp.Request) (*jwt.Token, error) {
	var token string
	var err error

	for _, method := range mw.TokenLookups {
		if len(token) > 0 {
			break
		}
		parts := strings.Split(strings.TrimSpace(method), ":")
		k := strings.TrimSpace(parts[0])
		v := strings.TrimSpace(parts[1])
		switch k {
		case "header":
			token, err = mw.jwtFromHeader(r, v)
		case "query":
			token, err = mw.jwtFromQuery(r, v)
		case "cookie":
			token, err = mw.jwtFromCookie(r, v)
		case "param":
			token, err = mw.jwtFromParam(r, v)
		}
	}

	if err != nil {
		return nil, err
	}

	if mw.KeyFunc != nil {
		return jwt.Parse(token, mw.KeyFunc)
	}

	return jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		if jwt.GetSigningMethod(mw.SigningAlgorithm) != t.Method {
			return nil, ErrInvalidSigningAlgorithm
		}
		if mw.usingPublicKeyAlgo() {
			return mw.publicKey, nil
		}

		// save token string if valid
		//r.SetParam(TokenKey, token)

		return mw.Key, nil
	})
}

func (mw *ManageJwt) parseTokenString(token string) (*jwt.Token, error) {
	if mw.KeyFunc != nil {
		return jwt.Parse(token, mw.KeyFunc)
	}

	return jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		if jwt.GetSigningMethod(mw.SigningAlgorithm) != t.Method {
			return nil, ErrInvalidSigningAlgorithm
		}
		if mw.usingPublicKeyAlgo() {
			return mw.publicKey, nil
		}

		return mw.Key, nil
	})
}

func (mw *ManageJwt) unauthorized(ctx context.Context, code int, message string) {
	r := g.RequestFromCtx(ctx)
	r.Header.Set("WWW-Authenticate", "JWT realm="+mw.Realm)
	mw.Unauthorized(ctx, code, message)
	if !mw.DisabledAbort {
		r.ExitAll()
	}
}

func (mw *ManageJwt) middlewareImpl(ctx context.Context) {
	r := g.RequestFromCtx(ctx)

	claims, token, err := mw.GetClaimsFromJWT(ctx)
	if err != nil {
		mw.unauthorized(ctx, http.StatusUnauthorized, mw.HTTPStatusMessageFunc(err, ctx))
		return
	}

	if claims["exp"] == nil {
		mw.unauthorized(ctx, http.StatusBadRequest, mw.HTTPStatusMessageFunc(ErrMissingExpField, ctx))
		return
	}

	if _, ok := claims["exp"].(float64); !ok {
		mw.unauthorized(ctx, http.StatusBadRequest, mw.HTTPStatusMessageFunc(ErrWrongFormatOfExp, ctx))
		return
	}

	if int64(claims["exp"].(float64)) < (mw.TimeFunc().UnixNano() / 1e6) {
		mw.unauthorized(ctx, http.StatusUnauthorized, mw.HTTPStatusMessageFunc(ErrExpiredToken, ctx))
		return
	}

	in, err := mw.inBlacklist(ctx, token)
	if err != nil {
		mw.unauthorized(ctx, http.StatusUnauthorized, mw.HTTPStatusMessageFunc(err, ctx))
		return
	}

	if in {
		mw.unauthorized(ctx, http.StatusUnauthorized, mw.HTTPStatusMessageFunc(ErrInvalidToken, ctx))
		return
	}

	r.SetParam(PayloadKey, claims)

	identity := mw.IdentityHandler(ctx)
	if identity != nil {
		//r.SetParam(mw.IdentityKey, identity)
	}

	if !mw.Authorizer(identity, ctx) {
		mw.unauthorized(ctx, http.StatusForbidden, mw.HTTPStatusMessageFunc(ErrForbidden, ctx))
		return
	}

	//c.Next() todo
}

func (mw *ManageJwt) setBlacklist(ctx context.Context, token string, claims jwt.MapClaims) error {
	// The goal of MD5 is to reduce the key length.
	token, err := gmd5.EncryptString(token)

	if err != nil {
		return err
	}

	exp := int64(claims["exp"].(float64))

	// save duration time = (exp + max_refresh) - now
	duration := time.Unix(exp, 0).Add(mw.MaxRefresh).Sub(mw.TimeFunc()).Truncate(time.Second)

	key := mw.BlacklistPrefix + token
	// global gcache
	err = blacklist.Set(ctx, key, true, duration)

	if err != nil {
		return err
	}

	return nil
}

func (mw *ManageJwt) inBlacklist(ctx context.Context, token string) (bool, error) {
	// The goal of MD5 is to reduce the key length.
	tokenRaw, err := gmd5.EncryptString(token)

	if err != nil {
		return false, nil
	}

	key := mw.BlacklistPrefix + tokenRaw
	// Global gcache
	if in, err := blacklist.Contains(ctx, key); err != nil {
		return false, nil
	} else {
		return in, nil
	}
}
