package util

import "github.com/jinzhu/copier"

var deepCopyOption = copier.Option{DeepCopy: true}

func DeepCopy(toValue interface{}, fromValue interface{}) error {
	return copier.CopyWithOption(toValue, fromValue, deepCopyOption)
}

func Copy(toValue interface{}, fromValue interface{}) error {
	return copier.Copy(toValue, fromValue)
}
