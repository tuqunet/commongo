package core

// BaseEntity 实体基类
type BaseEntity struct {
	innerStatus Status
}

// IsNew 是否新建
func (e *BaseEntity) IsNew() bool {
	return e.innerStatus.IsNew()
}

// IsPersist 是否持久化
func (e *BaseEntity) IsPersist() bool {
	return e.innerStatus.IsPersist()
}

// EntityStatus 设置实体状态
func (e *BaseEntity) EntityStatus(status Status) {
	e.innerStatus = status
}
