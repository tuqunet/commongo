package core

import "time"

// BaseDto 基础数据传输对象
type BaseDto struct {
	Id        uint64     //id
	CreatedAt *time.Time //创建时间
	UpdatedAt *time.Time //更新时间
}

type PageParam struct {
	Current  int `json:"current"`
	PageSize int `json:"pageSize"`
}

func (p *PageParam) InitPageParam() {
	if p.Current < 1 {
		p.Current = 1
	}
	if p.PageSize < 1 {
		p.PageSize = 10
	}
}

// PaginationDto 分页数据传输对象
type PaginationDto struct {
	Current  int `json:"current"`
	PageSize int `json:"pageSize"`
	Total    int `json:"total"`
}
