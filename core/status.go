package core

type Status string

const (
	// New  表示实体是刚创建状态,没有进行持久化
	New Status = "New"
	// Persist  表示实体是持久化状态
	Persist Status = "Persist"
)

// IsNew  判断实体是否是刚创建状态
func (e *Status) IsNew() bool {
	return *e == New
}

// IsPersist  判断实体是否是持久化状态
func (e *Status) IsPersist() bool {
	return *e == Persist
}
