package page

type PaginationReqDto struct {
	Current  int `json:"current" p:"current"`
	PageSize int `json:"pageSize" p:"pageSize"`
}

func (p *PaginationReqDto) PageReqCheckAndInit() {
	if p.PageSize <= 0 {
		p.PageSize = 20
	}
	if p.Current <= 0 {
		p.Current = 1
	}
}

func (p *PaginationReqDto) Offset() int {
	return p.PageSize * (p.Current - 1)
}

type PaginationResDto struct {
	Total    int64 `json:"total"`
	PageSize int   `json:"pageSize"`
	Current  int   `json:"current"`
}

type Page[T any] struct {
	Pagination PaginationResDto
	DataList   []T
}
