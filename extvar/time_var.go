package extvar

import (
	"strconv"
	"time"
)

// UnixMilliTime 用于json序列化时将日期以毫秒时间戳形式显示
type UnixMilliTime time.Time

func (t UnixMilliTime) MarshalJSON() ([]byte, error) {
	stamp := strconv.FormatInt(time.Time(t).UnixMilli(), 10)
	return []byte(stamp), nil
}
