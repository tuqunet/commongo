package extvar

import (
	"strconv"
	"testing"
	"time"
)

func TestUnixMilliTime_MarshalJSON(t *testing.T) {
	nowTime := time.Now()
	timestamp := nowTime.UnixMilli()
	vTime := UnixMilliTime(nowTime)

	timeMilli, _ := vTime.MarshalJSON()
	testMilli, err := strconv.Atoi(string(timeMilli))
	if err != nil {
		t.Fatal(err)
	}

	if timestamp != int64(testMilli) {
		t.Fatal("转换错误")
	}

}
