package file

type UploadReqDto struct {
	Action   string `json:"action"`
	FileName string `json:"fileName"`
}

type UploadResDto struct {
	FileKey string `json:"fileKey"`
	FileUrl string `json:"fileUrl"`
}
