package auth

import "time"

// OpDto 操作对象DTO
type OpDto interface {
	// GetActionTime 获取操作时间
	GetActionTime() *time.Time
}
