package auth

import "time"

// NtOpDto 非授权操作对象
type NtOpDto struct {
	ActionTime *time.Time
}

// NewNtOpDto 创建非授权操作对象
func NewNtOpDto() *NtOpDto {
	nowTime := time.Now()
	return &NtOpDto{
		ActionTime: &nowTime,
	}
}

// GetActionTime 获取操作时间
func (op *NtOpDto) GetActionTime() *time.Time {
	return op.ActionTime
}
